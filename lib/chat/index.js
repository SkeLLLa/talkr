'use strict';
var socket,
    chat,
    cookie = require('cookie'),
    log = require(__dirname + '/../logger'),
    model = require(__dirname + '/../../model'),
    db = require(__dirname + '/../db');

function getRoomMessages(room, userId, callback) {

}
function joinRoom(spark, room) {
  spark.join(room, function () {

    // send message to this client
    spark.send('chat.join', 'you joined room ' + room);

    // send message to all clients except this one
    spark.room(room).send('chat.join', spark.id + ' joined room ' + room);
  });
}
function sendMessage(spark, room, text) {
  var ss = new model.socket.session({connId: spark.conn.id});
  var msg = new model.chat.message({"room": room, "author": ss.userId, "text": text});
  msg.store(function (e, m) {
    if (!e) {
      spark.send('chat.message', m.room, m.author, m.text);
      spark.room(room).send('chat.message', m.room, m.author, m.text);
    } else {
      log.error('Failed to store message', e);
    }
  });

}

chat = function(){

};

chat.prototype.init = function(){
  if (socket){
    socket.on('connection', function (spark) {
      log.debug("Socket connected", spark.id, spark.conn.id, spark.headers.cookie);
      if (spark.headers.cookie) {
        var c = cookie.parse(spark.headers.cookie);
        var s = new model.socket.session({userId: c.auth, connId: spark.conn.id});
        s.store();
        var sUser = s;
        if (sUser) {
          var user = new model.socket.user({userId: sUser.userId, socketIds: [spark.id], connIds: [spark.conn.id]});
          user.store();
        } else {
          return;
        }
        spark.on('chat.join', function (room) {
          console.log('AAA');
          joinRoom(spark, room);
        });
        spark.on('chat.message', function (room, message) {
          sendMessage(spark, room, message);
        });
      } else {
        return;
      }
    });
  }
};

module.exports = function (opts) {
  if (opts){
    if (opts.socket){
      socket = opts.socket;
    }
  }
  var c = new chat();
  c.init();
  return c;
  //console.log('Chat loaded', opts);
};