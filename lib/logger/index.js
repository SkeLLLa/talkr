var config = require(__dirname + '/../config');

var Message = function(){
  "use strict";
  this.timestamp = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
  this.prefix = config.prefix;
  if (arguments[0]){
    this.text = arguments[0];
  }
  if (arguments[1]){
    this.info = arguments[1];
  }
};
Message.prototype.format = function(level){
  return this.prefix + ':(' + level.toUpperCase() + '):' + '[' + this.timestamp + ']' + ':' + '\"' + this.text + '\"' +
         (this.info ? ';' : '');
};
var logger = function(){
  "use strict";
  this.prefix = config.prefix;
  this.level = config.log.level;
  return this;
};

logger.prototype.info = function(message){
  "use strict";
  if (config.log.level <= config.log.levels.info){
    console.info(new Message(message, arguments).format("i"));
  }
};
logger.prototype.debug = function (message) {
  "use strict";
  if (config.log.level <= config.log.levels.info) {
    console.info(new Message(message, arguments).format("i"), "\n└DEBUG:", [].slice.call(arguments, 1));
  }
};
logger.prototype.warn = function(message, source){
  "use strict";
  if (config.log.level <= config.log.levels.warn){
    console.warn(new Message(message).format("w"), source);
  }
};
logger.prototype.error = function(message, exception){
  "use strict";
  if (config.log.level <= config.log.levels.error){
    console.error(new Message(message).format("e"), exception);
    console.trace();
  }
};
logger.prototype.assert = function(expression, failMessage){
  "use strict";
  if (config.log.level <= config.log.levels.debug){
    console.assert(expression, new Message(failMessage));
  }
};
logger.prototype.timer = function(action, label){
  "use strict";
  if (config.log.level <= config.log.levels.debug){
    switch (action){
      case "start":
        console.info(new Message('Timer \"' + label + '\" started.').format("d"));
        console.time(label);
        break;
      case "stop":
        console.timeEnd(label);
        console.info(new Message('Timer \"' + label + '\" stopped.').format("d"));
        break;
      default:
        break;
    }
  }
};

var a = new logger();
module.exports = a;
/*module.exports = function(){
 console.log("AAAAAAAAAAA");
 //return new logger();
 return a;
 };*/