'use strict';
function read() {
  var config;
  var opts = process.env.NODE_ENV;
  var fs = require('fs');
  if (opts === 'development') {
    config = JSON.parse(fs.readFileSync(__dirname + '/../../config/development.json', 'utf8'));
  }
  else if (opts === 'production') {
    config = JSON.parse(fs.readFileSync(__dirname + '/../../config/development.json', 'utf8'));
  }
  return config;
}
module.exports = read();