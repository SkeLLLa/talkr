var cb = require('./couchbase'),
    dummy = require('./dummy')();

module.exports.cb = cb.mainBucket;
module.exports.dummy = dummy;
