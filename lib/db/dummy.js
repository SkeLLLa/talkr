var dummy = {};
dummy.inc = function (key, callback) {
  if (dummy.hasOwnProperty(key)) {
    dummy[key]++;
  } else {
    dummy[key] = 1;
  }
  if (callback) {
    callback(dummy[key]);
  } else {
    return dummy[key];
  }
};

module.exports = function () {
  console.log("Dummy db init");
  return dummy;
};