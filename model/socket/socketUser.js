var db = require(__dirname + '/../../lib/db');

var socketUser = function (args) {
  if (args.userId && !args.socketIds && !args.connIds) {
    //try load
    return this.load(args.userId);
  }
  this.userId = args.userId || "";
  this.socketIds = args.socketIds || [];
  this.connIds = args.connIds || [];
  return this;
};

socketUser.prototype.store = function (callback) {
  if (!this.userId) { return; }
  var key = 'ws:user:' + this.userId;
  db.dummy[key] = this;
  if (typeof callback === "function") {
    callback(null, this);
  } else {
    return this;
  }
};
socketUser.prototype.load = function (id, callback) {
  if (!id) { return; }
  var key = 'ws:user:' + id;
  var result = db.dummy[key];
  if (typeof callback === "function") {
    if (result) {
      callback(null, result);
    } else {
      callback(new Error("Object not found"));
    }
  } else {
    return result;
  }
};
socketUser.prototype.update = function () {

};

module.exports = socketUser;