var db = require(__dirname + '/../../lib/db');

var socketSession = function (args) {
  if (!args.userId && args.connId) {
    //try load
    return this.load(args.connId);
  }
  this.userId = args.userId || "";
  this.connId = args.connId || "";
  return this;
};

socketSession.prototype.store = function (callback) {
  if (!this.userId) { return; }
  var key = 'ws:session:' + this.connId;
  db.dummy[key] = this;
  if (typeof callback === "function") {
    callback(null, this);
  } else {
    return this;
  }
};
socketSession.prototype.load = function (id, callback) {
  if (!id) { return; }
  var key = 'ws:session:' + id;
  var result = db.dummy[key];
  if (typeof callback === "function") {
    if (result) {
      callback(null, result);
    } else {
      callback(new Error("Object not found"));
    }
  } else {
    return result;
  }
};
socketSession.prototype.update = function () {

};

module.exports = socketSession;