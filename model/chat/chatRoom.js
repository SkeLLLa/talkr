'use strict';

var uuid = require('node-uuid'),
    db = require(__dirname + '/../../lib/db'),
    logPrefix = "model.chat.chatRoom";

var chatRoom = function (args, callback) {
  var self = this;
  if (args.id) {
    self.load(args.id, callback);
  }
  else if (args.topic && args.private && args.invited && args.participants && args.creator) {
    self.id = uuid.v4();
    self.topic = args.topic;
    self.type = "chat-room";
    self.private = args.private || true;
    self.timestamp = Date.now();
    self.invited = args.invited;
    self.participants = args.participants;
    self.creator = args.creator;
    if (typeof callback === 'function') {
      callback(null, self);
    }
  } else {
    if (typeof callback === 'function') {
      callback(null, self);
    }
  }
};

chatRoom.prototype.store = function (callback) {
  if (!this.id) { return; }
  var key = 'chat:room:' + this.id;
  db.dummy[key] = this;
  if (typeof callback === "function") {
    callback(null, this);
  } else {
    return this;
  }
};
chatRoom.prototype.load = function (id, callback) {
  if (!id) {
    if (typeof callback === "function") {
      callback(new Error("Failed to load " + logPrefix + ". No ID."));
    } else {
      throw new Error("Failed to load " + logPrefix + ".  No ID.");
    }
  }
  var key = 'chat:room:' + id;
  var result = db.dummy[key];
  if (typeof callback === "function") {
    if (result) {
      callback(null, result);
    } else {
      callback(new Error("Object not found"));
    }
  } else {
    return result;
  }
};

chatRoom.prototype.update = function (id, updates, callback) {
  if (!id) {
    if (typeof callback === "function") {
      callback(new Error("Failed to update " + logPrefix + ". No ID."));
    } else {
      throw new Error("Failed to update " + logPrefix + ".  No ID.");
    }
  }
  try {
    var key = 'chat:room:' + id;
    var result = db.dummy[key];
    updates.forEach(function (elem) {
      if (result.hasOwnProperty(elem)) { result[elem] = updates[elem]; }
      ;
    });
    //db.save
    db.dummy[key] = result;
    if (typeof callback === "function") {
      callback(null, result);
    } else {
      return result;
    }
  }
  catch (e) {
    if (typeof callback === "function") {
      callback(new Error("Failed to update " + logPrefix + "."));
    } else {
      throw new Error("Failed to update " + logPrefix + ".");
    }
  }
};

module.exports = chatRoom;