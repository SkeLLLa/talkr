var db = require(__dirname + '/../../lib/db'),
    logPrefix = "model.chat.chatRoom";

var chatMessage = function (args, callback) {
  var self = this;
  if (args.id && !args.author && !args.room) {
    self.load(args.id, callback);
  }
  else if (!args.id && args.author && args.room) {
    var counter = db.dummy.inc('chat:message:' + args.room + ':counter');
    self.id = counter; // create id (uuid or sequential per room)
    self.type = "chat:message";
    self.text = args.text || "";
    self.timestamp = Date.now();
    self.attaches = args.attaches; //[] - attachments
    self.author = args.author; // author id
    self.room = args.room; // destination room id
    self.visibility = args.visibility; // if 'null' - not visible, '[]' - visible to all, '[1,2]' - visible to users 1,2
    self.version = 0; // 0 - lastest, on update old version goes to self.version + 1, key changes to type:room:id:version
  }
};

chatMessage.prototype.load = function (args, callback) {

};

chatMessage.prototype.store = function (callback) {
  if (this.id && this.author && this.room) {
    var key = 'chat:message:' + this.room + ':' + this.id;
    db.dummy[key] = this;
    if (typeof callback === "function") {
      callback(null, this);
    } else {
      return this;
    }
  } else {
    if (typeof callback === 'function') {
      callback(new Error('Bad arguments'));
    } else {
      return;
    }
  }
};

module.exports = chatMessage;