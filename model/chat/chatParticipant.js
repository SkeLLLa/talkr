var chatParticipant = function (args, callback) {
  var self = this;
  self.id = args.id;
  self.notify = args.notify;
  self.readOnly = args.readOnly;
};