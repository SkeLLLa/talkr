'use strict';

var http = require('http'),
    connect = require('connect'),
    primusio = require('primus.io'),
    cookie = require('cookie'),
    chat,
    log = require('./lib/logger'),
    fs = require('fs'),
    model = require('./model'),
    db = require('./lib/db');

//noinspection JSUnresolvedFunction
var app = connect()
    .use(connect.logger('dev'))
    .use(connect.static('static'))
    .use(connect.cookieParser('iddqd'))
    .use(function (req, res) {
           res.setHeader('Content-Type', 'text/html');
           var u = db.dummy.inc('user:counter');
           //res.setHeader('auth', u);
           res.setHeader('Set-Cookie', 'auth=' + u + '; httponly; path="/";');
           //res.cookie('auth', u, { httpOnly: true }); //4 debug
           fs.createReadStream(__dirname + '/static/index.html').pipe(res);
           res.end();
         });

var server = http.createServer(app)
  , primus = new primusio(server, { transformer: 'engine.io', parser: 'JSON' });
primus.save('static/js/primus.js');
primus.authorize(function (req, done) {
  if (req.headers && req.headers.cookie) {
    var c = cookie.parse(req.headers.cookie);
    if (c.auth) {
      done(null, c.auth); //TODO: proper auth check
    } else {
      done(new Error("Auth failed"));
      /*var s = new model.socket.session({userId: 1, connId: c.io});
       s.store(done);*/
    }
  } else {
    done(new Error("Auth failed"));
  }

});
var chatSocket = primus.channel('chat');
chat = require('./lib/chat')({socket: chatSocket});


server.listen(3000);